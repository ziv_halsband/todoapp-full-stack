import { Router } from "express"
import { getTodo, getTodos, addTodo, updateTodo, deleteTodo } from "../controllers/todos"

const router: Router = Router()

router.get("/todos", getTodos)
router.get("/todos/:id", getTodo)
router.post("/todos", addTodo)
router.put("/todos/:id", updateTodo)
router.delete("/todos/:id", deleteTodo)

export default router