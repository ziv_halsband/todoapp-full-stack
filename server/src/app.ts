import express, { Express } from "express"
import mongoose from "mongoose"
import cors from "cors"
import todoRoutes from "./routes/todos-router"
import { json } from 'body-parser';

const app: Express = express()

const PORT: string | number = process.env.PORT || 4000

app.use(json());
app.use(cors())
app.use(todoRoutes)

const uri: string = `mongodb://bigid:password@localhost:27017/todos?authSource=admin`


mongoose
  .connect(uri)
  .then(() =>
    app.listen(PORT, () =>
      console.log(`Server running on http://localhost:${PORT}`)
    )
  )
  .catch(error => {
    throw error
  })